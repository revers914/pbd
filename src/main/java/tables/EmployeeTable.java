package tables;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.commons.collections4.iterators.EmptyOrderedIterator;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;
import java.util.StringJoiner;

public class EmployeeTable extends Table {

    public EmployeeTable(ResultSet rs) {
        columnsMap = new LinkedHashMap<>();
        columnsMap.put("id", "Код сотрудника");
        columnsMap.put("firstName", "Имя");
        columnsMap.put("lastName", "Фамилия");
        columnsMap.put("position", "Должность");
        init(rs);
    }

    protected void initColumns() {
        createColumns(columnsMap);
    }

    protected TableObject createObjects(ResultSet rs) throws SQLException {
        return new Employee(
                rs.getInt(columnsMap.get("id")),
                rs.getString(columnsMap.get("firstName")),
                rs.getString(columnsMap.get("lastName")),
                rs.getString(columnsMap.get("position"))
        );
    }

    public static class Employee implements TableObject {
        private StringProperty id;
        private StringProperty firstName;
        private StringProperty lastName;
        private StringProperty position;

        public Employee() {

        }

        @Override
        public String toString() {
            return String.format("Employee, id=%s, firstName=%s, lastName=%s, position=%s",
                    id.get(), firstName.get(), lastName.get(), position.get()
            );
        }

        public Employee(Integer id, String firstName, String lastName, String position) {
            idProperty().setValue(id.toString());
            firstNameProperty().setValue(firstName);
            lastNameProperty().setValue(lastName);
            positionProperty().setValue(position);
        }

        public StringProperty idProperty() {
            if (id == null) id = new SimpleStringProperty(this, "id");
            return id;
        }

        public StringProperty firstNameProperty() {
            if (firstName == null) firstName = new SimpleStringProperty(this, "firstName");
            return firstName;
        }

        public StringProperty lastNameProperty() {
            if (lastName == null) lastName = new SimpleStringProperty(this, "lastName");
            return lastName;
        }

        public StringProperty positionProperty() {
            if (position == null) position = new SimpleStringProperty(this, "position");
            return position;
        }

        @Override
        public int insertObject(Connection con) throws SQLException {
            return 0;
        }

        @Override
        public int deleteObject(Connection con) throws SQLException {
            return 0;
        }

        @Override
        public int updateObject(Connection con) throws SQLException {
            return 0;
        }
    }
}
