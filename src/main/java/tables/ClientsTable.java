package tables;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;

public class ClientsTable extends Table {

    public ClientsTable(ResultSet rs) {
        columnsMap = new LinkedHashMap<>(7);
        columnsMap.put("id", "Код клиента");
        columnsMap.put("name", "Название");
        columnsMap.put("accountAddress", "Адрес счета");
        columnsMap.put("city", "Город");
        columnsMap.put("phoneNumber", "Телефон");
        columnsMap.put("postIndex", "Индекс");
        columnsMap.put("country", "Страна");
        init(rs);
    }


    @Override
    protected void initColumns() {
        createColumns(columnsMap);
    }

    protected TableObject createObjects(ResultSet rs) throws SQLException {
        return new Client(
                rs.getInt(columnsMap.get("id")),
                rs.getString(columnsMap.get("name")),
                rs.getString(columnsMap.get("accountAddress")),
                rs.getString(columnsMap.get("city")),
                rs.getString(columnsMap.get("phoneNumber")),
                rs.getString(columnsMap.get("postIndex")),
                rs.getString(columnsMap.get("country"))
        );
    }

    public static class Client implements TableObject {
        private StringProperty id;
        private StringProperty name;
        private StringProperty accountAddress;
        private StringProperty city;
        private StringProperty phoneNumber;
        private StringProperty postIndex;
        private StringProperty country;

        public Client(Integer id, String name, String accountAddress, String city,
                      String phoneNumber, String postIndex, String country) {
            idProperty().setValue(id.toString());
            nameProperty().setValue(name);
            accountAddressProperty().setValue(accountAddress);
            cityProperty().setValue(city);
            phoneNumberProperty().setValue(phoneNumber);
            postIndexProperty().setValue(postIndex);
            countryProperty().setValue(country);
        }

        public StringProperty idProperty() {
            if (id == null) id = new SimpleStringProperty(this, "id");
            return id;
        }

        public StringProperty nameProperty() {
            if (name == null) name = new SimpleStringProperty(this, "name");
            return name;
        }

        public StringProperty accountAddressProperty() {
            if (accountAddress == null) accountAddress = new SimpleStringProperty(this, "accountAddress");
            return accountAddress;
        }

        public StringProperty cityProperty() {
            if (city == null) city = new SimpleStringProperty(this, "city");
            return city;
        }

        public StringProperty phoneNumberProperty() {
            if (phoneNumber == null) phoneNumber = new SimpleStringProperty(this, "phoneNumber");
            return phoneNumber;
        }

        public StringProperty postIndexProperty() {
            if (postIndex == null) postIndex = new SimpleStringProperty(this, "postIndex");
            return postIndex;
        }

        public StringProperty countryProperty() {
            if (country == null) country = new SimpleStringProperty(this, "postIndex");
            return country;
        }

        @Override
        public int insertObject(Connection con) throws SQLException {
            PreparedStatement insert = con.prepareStatement("insert into Клиенты values (?,?,?,?,?,?,?)");
            insert.setInt(1, Integer.parseInt(id.get()));
            insert.setString(2, name.get());
            insert.setString(3, accountAddress.get());
            insert.setString(4, city.get());
            insert.setString(5, phoneNumber.get());
            insert.setString(6, postIndex.get());
            insert.setString(7, country.get());
            return insert.executeUpdate();
        }

        @Override
        public int deleteObject(Connection con) throws SQLException {
            return 0;
        }

        @Override
        public int updateObject(Connection con) throws SQLException {
            return 0;
        }
    }

}
