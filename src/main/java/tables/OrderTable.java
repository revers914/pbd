package tables;

import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;

public class OrderTable extends Table {

    public OrderTable(ResultSet rs) {
        columnsMap = new LinkedHashMap<>();
        columnsMap.put("orderId", "Номер заказа");
        columnsMap.put("orderDate", "Дата заказа");
        columnsMap.put("employeeId", "Код сотрудника");
        columnsMap.put("clientId", "Код клиента");
        columnsMap.put("serialNumber", "Серийный номер");
        columnsMap.put("completionDate", "Дата завершения");
        columnsMap.put("taxRate", "Ставка налога");
        init(rs);
    }

    protected void initColumns() {
        createColumns(columnsMap);
    }

    protected TableObject createObjects(ResultSet rs) throws SQLException {
        return new Order(
                rs.getInt(columnsMap.get("orderId")),
                rs.getString(columnsMap.get("orderDate")),
                rs.getInt(columnsMap.get("employeeId")),
                rs.getInt(columnsMap.get("clientId")),
                rs.getString(columnsMap.get("serialNumber")),
                rs.getString(columnsMap.get("completionDate")),
                rs.getInt(columnsMap.get("taxRate"))
        );
    }

    public static class Order implements TableObject {
        private StringProperty orderId;
        private StringProperty orderDate;
        private StringProperty employeeId;
        private StringProperty clientId;
        private StringProperty serialNumber;
        private StringProperty completionDate;
        private StringProperty taxRate;


        public Order(Integer orderId, String orderDate, Integer employeeId, Integer clientId,
                     String serialNumber, String completionDate, Integer taxRate) {
            orderIdProperty().setValue(orderId.toString());
            orderDateProperty().setValue(orderDate);
            employeeIdProperty().setValue(employeeId.toString());
            clientIdProperty().setValue(clientId.toString());
            serialNumberProperty().setValue(serialNumber);
            completionDateProperty().setValue(completionDate);
            taxRateProperty().setValue(taxRate.toString());
        }

        public StringProperty orderIdProperty() {
            if (orderId == null) orderId = new SimpleStringProperty(this, "orderId");
            return orderId;
        }

        public StringProperty orderDateProperty() {
            if (orderDate == null) orderDate = new SimpleStringProperty(this, "orderDate");
            return orderDate;
        }

        public StringProperty employeeIdProperty() {
            if (employeeId == null) employeeId = new SimpleStringProperty(this, "employeeId");
            return employeeId;
        }

        public StringProperty clientIdProperty() {
            if (clientId == null) clientId = new SimpleStringProperty(this, "clientId");
            return clientId;
        }

        public StringProperty serialNumberProperty() {
            if (serialNumber == null) serialNumber = new SimpleStringProperty(this, "serialNumber");
            return serialNumber;
        }

        public StringProperty completionDateProperty() {
            if (completionDate == null) completionDate = new SimpleStringProperty(this, "completionDate");
            return completionDate;
        }

        public StringProperty taxRateProperty() {
            if (taxRate == null) taxRate = new SimpleStringProperty(this, "taxRate");
            return taxRate;
        }

        @Override
        public int insertObject(Connection con) throws SQLException {
            return 0;
        }

        @Override
        public int deleteObject(Connection con) throws SQLException {
            return 0;
        }

        @Override
        public int updateObject(Connection con) throws SQLException {
            return 0;
        }
    }


}
