package tables;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.Property;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TableColumn;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static javafx.scene.control.cell.TextFieldTableCell.forTableColumn;

public abstract class Table {
    Map<String, String> columnsMap;
    private List<TableColumn<TableObject, String>> columns = new ArrayList<>();
    private ObservableList<TableObject> items = FXCollections.observableArrayList();

    public List<TableColumn<TableObject, String>> getColumns() {
        return columns;
    }

    public List<TableObject> getItems() {
        return items;
    }

    private void clear() {
        columns.clear();
        items.clear();
    }

    void init(ResultSet rs) {
        try {
            clear();
            while (rs.next()) {
                items.add(createObjects(rs));
            }
            initColumns();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public Map<String, String> getMap() {
        return columnsMap;
    }

    void createColumns(Map<String, String> columnsMap) {
        columnsMap.forEach((key, value) -> {
            TableColumn<TableObject, String> column = new TableColumn<>(value);
            column.setCellValueFactory(new PropertyValueFactory<>(key));
            column.setCellFactory(TextFieldTableCell.forTableColumn());
            columns.add(column);
        });
    }

    abstract protected void initColumns();

    abstract protected TableObject createObjects(ResultSet rs) throws SQLException;

    public interface TableObject {
        int insertObject(Connection con) throws SQLException;

        int deleteObject(Connection con) throws SQLException;

        int updateObject(Connection con) throws SQLException;
    }
}
