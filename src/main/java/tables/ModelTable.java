package tables;


import javafx.beans.property.IntegerProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedHashMap;

public class ModelTable extends Table {

    public ModelTable(ResultSet rs) {
        columnsMap = new LinkedHashMap<>();
        columnsMap.put("name", "Модель");
        columnsMap.put("description", "Описание");
        columnsMap.put("yearOfIssue", "Выпуск");
        init(rs);
    }

    protected void initColumns() {
        createColumns(columnsMap);
    }

    protected TableObject createObjects(ResultSet rs) throws SQLException {
        return new Model(
                rs.getString(columnsMap.get("name")),
                rs.getString(columnsMap.get("description")),
                rs.getInt(columnsMap.get("yearOfIssue"))
        );
    }

    public static class Model implements TableObject {
        private StringProperty name;
        private StringProperty description;
        private StringProperty yearOfIssue;

        public Model(String name, String description, Integer yearOfIssue) {
            nameProperty().setValue(name);
            descriptionProperty().setValue(description);
            yearOfIssueProperty().setValue(yearOfIssue.toString());
        }

        public StringProperty nameProperty() {
            if (name == null) name = new SimpleStringProperty(this, "name");
            return name;
        }

        public StringProperty descriptionProperty() {
            if (description == null) description = new SimpleStringProperty(this, "description");
            return description;
        }

        public StringProperty yearOfIssueProperty() {
            if (yearOfIssue == null) yearOfIssue = new SimpleStringProperty(this, "yearOfIssue");
            return yearOfIssue;
        }

        @Override
        public int insertObject(Connection con) throws SQLException {
            return 0;
        }

        @Override
        public int deleteObject(Connection con) throws SQLException {
            return 0;
        }

        @Override
        public int updateObject(Connection con) throws SQLException {
            return 0;
        }
    }
}
