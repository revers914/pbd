package util;

import com.mysql.jdbc.jdbc2.optional.MysqlDataSource;
import javafx.beans.property.ObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import tables.*;
import tables.Table.TableObject;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

public class Requester {
    private GridPane gp = new GridPane();
    private Table dataTable;
    private List<TableColumn<TableObject, String>> columns = new ArrayList<>();
    private ObservableList<TableObject> items = FXCollections.observableArrayList();
    private MysqlDataSource ds = new MysqlDataSource();

    public Requester() {

    }

    public Table getDataTable() {
        return dataTable;
    }

    public void setUser(String server) {
        switch (server) {
            case "Raspi":
                String RASPI_USER = "reversor";
                ds.setUser(RASPI_USER);
                String RASPI_PASSWORD = "revers914";
                ds.setPassword(RASPI_PASSWORD);
                String RASPI_URL = "jdbc:mysql://92.241.240.42:3306/Order?characterEncoding=UTF-8&useSSL=false";
                ds.setURL(RASPI_URL);
                break;
            case "Local":
                String user = "root";
                ds.setUser(user);
                String password = "Revers914";
                ds.setPassword(password);
                String URL = "jdbc:mysql://127.0.0.1:3306/Order?characterEncoding=UTF-8&useSSL=false";
                ds.setUrl(URL);
                break;
        }
    }

    public List<TableColumn<TableObject, String>> getColumns() {
        return columns;
    }


    public ObservableList<TableObject> getItems() {
        return items;
    }

    public GridPane getGp() {
        return gp;
    }

    private void setGridPane() {
        gp.getChildren().clear();
        List<Map.Entry<String, String>> desc = new ArrayList<>(dataTable.getMap().entrySet());
        IntStream.range(0, desc.size()).forEach(idx -> {
            Label l = new Label(desc.get(idx).getValue());
            GridPane.setConstraints(l, 0, idx);
            TextField tf = new TextField();
            tf.setId(desc.get(idx).getKey());
            GridPane.setConstraints(tf, 1, idx);
            gp.getChildren().addAll(l, tf);
        });
    }

    public void loadTable(String tableName) {
        try (
                Connection conn = ds.getConnection();
                Statement st = conn.createStatement()
        ) {
            Class.forName("com.mysql.jdbc.Driver");

            ResultSet rs = st.executeQuery("select * from " + tableName);

            columns.clear();
            items.clear();

            switch (tableName) {
                case "Сотрудники":
                    dataTable = new EmployeeTable(rs);
                    break;
                case "Клиенты":
                    dataTable = new ClientsTable(rs);
                    break;
                case "Модели":
                    dataTable = new ModelTable(rs);
                    break;
                case "Заказы":
                    dataTable = new OrderTable(rs);
                    break;
            }

            columns.addAll(dataTable.getColumns());
            items.addAll(dataTable.getItems());
            setGridPane();

        } catch (SQLException | ClassNotFoundException | NullPointerException e) {
            e.printStackTrace();
        }
    }

    public void insertObject(TableObject object) {
        try (
                Connection conn = ds.getConnection()
        ) {
            Class.forName("com.mysql.jdbc.Driver");

            object.insertObject(conn);

        } catch (SQLException | ClassNotFoundException | NullPointerException e) {
            e.printStackTrace();
        }
    }

}
