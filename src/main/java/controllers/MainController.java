package controllers;

import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.Pane;
import tables.EmployeeTable;
import tables.Table;
import tables.Table.TableObject;
import util.Requester;

import java.io.IOException;
import java.util.Arrays;

public class MainController {
    private Requester requester;

    @FXML
    private ChoiceBox<String> user;

    @FXML
    private Pane pane;

    @FXML
    private TableView<TableObject> table;

    @FXML
    void userChoosed() {
        requester.setUser(user.getValue());
    }

    @FXML
    void tableChoosed(MouseEvent event) throws IOException {

        Button b = (Button) event.getSource();

        requester.loadTable(b.getText());
        table.setId(b.getText());
        table.setItems(requester.getItems());
        table.getColumns().clear();
        table.getColumns().addAll(requester.getColumns());

        table.getItems().forEach(System.out::println);

        pane.getChildren().clear();
        pane.getChildren().add(requester.getGp());
    }

    @FXML
    void send(MouseEvent event) {

    }

    @FXML
    void pressed(KeyEvent event) {
    }

    @FXML
    void actionButtonClicked(MouseEvent event) {
        String buttonName = ((Button) event.getSource()).getText();
        switch (buttonName) {
            case "Insert":
                /*Table.TableObject object;
                requester.getGp().getChildren().forEach(r -> {
                            if (r instanceof TextField) {
                                switch (table.getId()) {
                                    case "Сотрудники":
                                        break;
                                    case "Заказы":
                                        break;
                                    case "Модели":
                                        break;
                                    case "Клиенты":
                                        break;
                                }
                                ((TextField)r).getId();
                            }
                        }
                );*/
                break;
            case "Update":
                break;
            case "Remove":
                break;
        }
    }

    @FXML
    private void initialize() {
        requester = new Requester();
        user.setValue("Local");
        user.setItems(FXCollections.observableList(Arrays.asList("Raspi", "Local")));
        requester.setUser(user.getValue());
    }
}
